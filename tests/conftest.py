import logging
from datetime import datetime

import pytest
from alembic import command
from alembic.config import Config

from app import create_app
from app.config import TestingConfig
from app.db import db as _db
from app.models import Client, ClientParking, Parking

logging.basicConfig(level=logging.INFO)


@pytest.fixture(scope="session")
def app():
    _app = create_app(TestingConfig)
    yield _app


@pytest.fixture
def client(app):
    with app.app_context():
        yield app.test_client()


@pytest.fixture
def db(app):
    with app.app_context():
        alembic_cfg = Config()
        alembic_cfg.set_main_option("script_location", "migrations")
        alembic_cfg.set_main_option(
            "sqlalchemy.url", TestingConfig.SQLALCHEMY_DATABASE_URI
        )

        try:
            command.downgrade(alembic_cfg, "base")
            command.upgrade(alembic_cfg, "head")
        except Exception as e:
            logging.error(f"Error during migration: {e}")
            raise

        client_1 = Client(
            name="Ivan", surname="Ivanov", credit_card="i123", car_number="I123I"
        )
        client_2 = Client(
            name="Max", surname="Maximov", credit_card="m123", car_number="M123M"
        )
        client_3 = Client(name="Olga", surname="Maximova", car_number="O123O")
        parking = Parking(
            address="Lenina, 1",
            opened=True,
            count_places=100,
            count_available_places=50,
        )
        parking_full = Parking(
            address="Gagarina, 2",
            opened=True,
            count_places=100,
            count_available_places=0,
        )
        parking_closed = Parking(
            address="Lomonosova, 21",
            opened=False,
            count_places=100,
            count_available_places=2,
        )
        _db.session.add(client_1)
        _db.session.add(client_2)
        _db.session.add(client_3)
        _db.session.add(parking)
        _db.session.add(parking_full)
        _db.session.add(parking_closed)
        _db.session.commit()

        client_parking_1 = ClientParking(
            client_id=client_1.id, parking_id=parking.id, time_in=datetime.now()
        )
        client_parking_2 = ClientParking(
            client_id=client_3.id, parking_id=parking.id, time_in=datetime.now()
        )
        _db.session.add(client_parking_1)
        _db.session.add(client_parking_2)
        _db.session.commit()

        yield _db

        _db.session.remove()
        command.downgrade(alembic_cfg, "base")
