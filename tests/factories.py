import random

from factory import LazyAttribute
from factory.alchemy import SQLAlchemyModelFactory
from faker import Faker

from app.db import db
from app.models import Client, Parking

faker = Faker()


class ClientFactory(SQLAlchemyModelFactory):
    class Meta:
        model = Client
        sqlalchemy_session = db.session

    name = LazyAttribute(lambda _: faker.first_name())
    surname = LazyAttribute(lambda _: faker.last_name())
    credit_card = LazyAttribute(
        lambda _: faker.bothify(text="????????????")
        if random.choice([True, False])
        else None
    )
    car_number = LazyAttribute(lambda _: faker.license_plate())


class ParkingFactory(SQLAlchemyModelFactory):
    class Meta:
        model = Parking
        sqlalchemy_session = db.session

    address = LazyAttribute(lambda _: faker.address().replace("\n", ", "))
    opened = LazyAttribute(lambda _: random.choice([True, False]))
    count_places = LazyAttribute(lambda _: faker.random_int(min=50, max=500))
    count_available_places = LazyAttribute(
        lambda obj: faker.random_int(min=0, max=obj.count_places)
    )
