import pytest

BASE_CLIENT_PARKING_URL = "/api/v1/client-parking/"
BASE_PARKING_URL = "/api/v1/parking/"


@pytest.mark.entry
def test_entry_client_not_found(client, db):
    data = {"client_id": 999, "parking_id": 1}
    response = client.post(BASE_CLIENT_PARKING_URL, json=data)
    new_entry = response.get_json()
    assert response.status_code == 404
    assert new_entry.get("message") == "Client with ID 999 not found"


@pytest.mark.entry
def test_entry_parking_not_found(client, db):
    data = {"client_id": 2, "parking_id": 999}
    response = client.post(BASE_CLIENT_PARKING_URL, json=data)
    new_entry = response.get_json()
    assert response.status_code == 404
    assert new_entry.get("message") == "Parking with ID 999 not found"


@pytest.mark.entry
def test_entry_parking_closed(client, db):
    data = {"client_id": 2, "parking_id": 3}
    response = client.post(BASE_CLIENT_PARKING_URL, json=data)
    new_entry = response.get_json()
    assert response.status_code == 400
    assert new_entry.get("message") == "Parking is closed"


@pytest.mark.entry
def test_entry_no_available_places(client, db):
    data = {"client_id": 2, "parking_id": 2}
    response = client.post(BASE_CLIENT_PARKING_URL, json=data)
    new_entry = response.get_json()
    assert response.status_code == 400
    assert new_entry.get("message") == "No available places on parking"


@pytest.mark.entry
def test_entry_client_already_parked(client, db):
    data = {"client_id": 1, "parking_id": 1}
    response = client.post(BASE_CLIENT_PARKING_URL, json=data)
    new_entry = response.get_json()
    assert response.status_code == 400
    assert new_entry.get("message") == "Client is already on the parking"


@pytest.mark.entry
def test_entry_success(client, db):
    data = {"client_id": 2, "parking_id": 1}

    parking = client.get(BASE_PARKING_URL + "1").get_json()
    assert parking.get("count_available_places") == 50

    response = client.post(BASE_CLIENT_PARKING_URL, json=data)
    new_entry = response.get_json()

    assert response.status_code == 201
    assert new_entry["client_id"] == data["client_id"]
    assert new_entry["parking_id"] == data["parking_id"]
    assert new_entry["time_in"] is not None
    assert new_entry["time_out"] is None

    parking = client.get(BASE_PARKING_URL + "1").get_json()
    assert parking.get("count_available_places") == 49
