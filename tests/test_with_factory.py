from tests.factories import ClientFactory, ParkingFactory

BASE_CLIENTS_URL = "/api/v1/clients/"
BASE_PARKING_URL = "/api/v1/parking/"


def test_create_client_with_factory(client, db):
    new_client = ClientFactory()
    clients = client.get(BASE_CLIENTS_URL).get_json()

    assert new_client.id is not None
    assert new_client.name is not None
    assert new_client.surname is not None
    assert new_client.car_number is not None
    assert len(clients) == 4


def test_create_parking_with_factory(client, db):
    new_parking = ParkingFactory()
    parking_list = client.get(BASE_PARKING_URL).get_json()

    assert new_parking.id is not None
    assert new_parking.address is not None
    assert new_parking.opened is not None
    assert new_parking.count_places is not None
    assert new_parking.count_available_places is not None
    assert len(parking_list) == 4
