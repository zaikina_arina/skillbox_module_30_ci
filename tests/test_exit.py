import pytest

BASE_CLIENT_PARKING_URL = "/api/v1/client-parking/"
BASE_PARKING_URL = "/api/v1/parking/"


@pytest.mark.exit
def test_exit_client_not_found(client, db):
    data = {"client_id": 999, "parking_id": 1}
    response = client.delete(BASE_CLIENT_PARKING_URL, json=data)
    new_exit = response.get_json()
    assert response.status_code == 404
    assert new_exit.get("message") == "Client with ID 999 not found"


@pytest.mark.exit
def test_exit_parking_not_found(client, db):
    data = {"client_id": 2, "parking_id": 999}
    response = client.delete(BASE_CLIENT_PARKING_URL, json=data)
    new_exit = response.get_json()
    assert response.status_code == 404
    assert new_exit.get("message") == "Parking with ID 999 not found"


@pytest.mark.exit
def test_exit_client_not_parked(client, db):
    data = {"client_id": 1, "parking_id": 2}
    response = client.delete(BASE_CLIENT_PARKING_URL, json=data)
    new_exit = response.get_json()
    assert response.status_code == 400
    assert new_exit.get("message") == "Client is not on the parking"


@pytest.mark.exit
def test_exit_client_no_credit_card(client, db):
    data = {"client_id": 3, "parking_id": 1}

    response = client.delete(BASE_CLIENT_PARKING_URL, json=data)
    new_exit = response.get_json()
    assert response.status_code == 400
    assert new_exit.get("message") == "Client does not have a linked card"


@pytest.mark.exit
def test_exit_success(client, db):
    data = {"client_id": 1, "parking_id": 1}

    parking = client.get(BASE_PARKING_URL + "1").get_json()
    assert parking.get("count_available_places") == 50

    response = client.delete(BASE_CLIENT_PARKING_URL, json=data)
    new_exit = response.get_json()

    assert response.status_code == 200
    assert new_exit["client_id"] == data["client_id"]
    assert new_exit["parking_id"] == data["parking_id"]
    assert new_exit["time_in"] is not None
    assert new_exit["time_out"] is not None
    assert new_exit["time_in"] < new_exit["time_out"]

    parking = client.get(BASE_PARKING_URL + "1").get_json()
    assert parking.get("count_available_places") == 51
