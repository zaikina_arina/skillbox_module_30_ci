BASE_PARKING_URL = "/api/v1/parking/"


def test_create_parking(client, db):
    data = {
        "address": "Sizova, 1",
        "opened": True,
        "count_places": 100,
        "count_available_places": 50,
    }

    response = client.post(BASE_PARKING_URL, json=data)
    new_parking = response.get_json()
    parking_list = client.get(BASE_PARKING_URL).get_json()

    assert response.status_code == 201
    assert new_parking["address"] == data["address"]
    assert new_parking["opened"] == data["opened"]
    assert new_parking["count_places"] == data["count_places"]
    assert new_parking["count_available_places"] == data["count_available_places"]
    assert len(parking_list) == 4
    assert parking_list[3].get("id") == 4


def test_create_existing_parking(client, db):
    data = {
        "address": "Lenina, 1",
        "opened": True,
        "count_places": 100,
        "count_available_places": 50,
    }
    response = client.post(BASE_PARKING_URL, json=data)
    new_client = response.get_json()

    assert response.status_code == 409
    assert new_client.get("message") == "Parking already exists"


def test_create_parking_bad_request(client, db):
    data = {"address": "Lenina, 10", "opened": True}

    response = client.post(BASE_PARKING_URL, json=data)
    new_parking = response.get_json()

    assert response.status_code == 400
    assert new_parking.get("message") == "Input payload validation failed"
