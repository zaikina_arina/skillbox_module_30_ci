BASE_CLIENTS_URL = "/api/v1/clients/"


def test_create_client(client, db):
    data = {
        "name": "Petr",
        "surname": "Petrov",
        "credit_card": "p123",
        "car_number": "P123P",
    }

    response = client.post(BASE_CLIENTS_URL, json=data)
    new_client = response.get_json()
    clients = client.get(BASE_CLIENTS_URL).get_json()

    assert response.status_code == 201
    assert new_client["name"] == data["name"]
    assert new_client["surname"] == data["surname"]
    assert new_client["credit_card"] == data["credit_card"]
    assert new_client["car_number"] == data["car_number"]
    assert len(clients) == 4
    assert clients[3].get("id") == 4


def test_create_existing_client(client, db):
    data = {
        "name": "Ivan",
        "surname": "Ivanov",
        "credit_card": "i123",
        "car_number": "I123I",
    }
    response = client.post(BASE_CLIENTS_URL, json=data)
    new_client = response.get_json()

    assert response.status_code == 409
    assert new_client.get("message") == "Client already exists"


def test_create_client_bad_request(client, db):
    data = {"name": "Anna", "surname": "Petrova"}
    response = client.post(BASE_CLIENTS_URL, json=data)
    new_client = response.get_json()

    assert response.status_code == 400
    assert new_client.get("message") == "Input payload validation failed"
