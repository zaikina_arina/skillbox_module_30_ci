import pytest


@pytest.mark.parametrize(
    "endpoint",
    [
        "/api/v1/clients/",
        "/api/v1/clients/1",
        "/api/v1/parking/",
        "/api/v1/parking/1",
        "/api/v1/client-parking/",
    ],
)
def test_get_methods(client, endpoint, db):
    response = client.get(endpoint)
    assert response.status_code == 200
