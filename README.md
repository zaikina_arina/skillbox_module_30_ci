## Запуск проекта

1. Переименовать файл `.env.template` в `.env` и заполнить актуальными данными

```
POSTGRES_USER=
POSTGRES_DB=parking
POSTGRES_TEST_DB=parking_test
POSTGRES_PORT=5432
POSTGRES_HOST=db
POSTGRES_PASSWORD=
```

2. Запустить контейнеры

```bash
docker-compose up -d
```

3. Зайти в контейнер приложения

```bash
docker exec -it app_parking /bin/bash
```

4. Применить миграции

```bash
alembic upgrade head
```

5. Запустить тесты

```bash
pytest -v --disable-warnings
```

6. Ручки доступны по ссылке
   http://localhost:5000/

*Примечание:*

Для скрипта, создающего тестовую БД, давала разрешение на выполнение
(из корневой папки `hw`)

```bash
chmod +x ./db/init-db.sh
```
