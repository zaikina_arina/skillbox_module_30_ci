from typing import Any, Dict, List, Tuple

from flask import g, request
from flask_restx import Resource, fields

from app.namespaces import client_namespace

client_post_model = client_namespace.model(
    "ClientPostModel",
    {
        "name": fields.String(required=True, description="Client name"),
        "surname": fields.String(required=True, description="Client surname"),
        "credit_card": fields.String(
            required=True, description="Credit card number of the client"
        ),
        "car_number": fields.String(
            required=True, description="Car number of the client"
        ),
    },
)

client_get_model = client_namespace.clone(
    "ClientGetModel",
    client_post_model,
    {
        "id": fields.Integer(
            readOnly=True, description="The unique identifier of a client"
        ),
    },
)


class ClientListResource(Resource):
    @client_namespace.doc(
        description="Создание нового клиента",
        responses={
            201: ("Клиент успешно создан", client_post_model),
            400: "Неверные данные клиента",
            409: "Клиент уже существует",
            500: "Внутренняя ошибка сервера",
        },
    )
    @client_namespace.expect(client_post_model, validate=True)
    def post(self) -> Tuple[Dict[str, Any], int]:
        """Создание клиента"""
        client_data = request.get_json()

        response, status = g.client_service.create_client(client_data)
        return response, status

    @client_namespace.doc(
        description="Просмотр всех клиентов",
        responses={
            200: ("Информация о клиентах успешна возвращена", [client_get_model]),
            500: "Внутренняя ошибка сервера",
        },
    )
    def get(self) -> Tuple[List[Dict[str, Any]], int]:
        """Просмотр всех клиентов"""
        response, status = g.client_service.get_all_clients()
        return response, status


class ClientResource(Resource):
    @client_namespace.doc(
        params={"client_id": "ID клиента"},
        description="Информация о клиенте по ID",
        responses={
            200: ("Информация о клиенте успешно возвращена", client_get_model),
            404: "Клиент не найден",
            500: "Внутренняя ошибка сервера",
        },
    )
    def get(self, client_id: int) -> Tuple[List[Dict[str, Any]], int]:
        """Просмотр клиента по ID"""
        response, status = g.client_service.get_client_by_id(client_id)
        return response, status
