from typing import Any, Dict, List, Tuple

from flask import g, request
from flask_restx import Resource, fields

from app.namespaces import client_parking_namespace

client_parking_post_model = client_parking_namespace.model(
    "ClientParkingPostModel",
    {
        "client_id": fields.Integer(required=True, description="Client ID"),
        "parking_id": fields.Integer(required=True, description="Parking ID"),
    },
)

client_parking_get_model = client_parking_namespace.clone(
    "ClientParkingGetModel",
    client_parking_post_model,
    {
        "id": fields.Integer(
            readOnly=True, description="The unique identifier of the client parking"
        ),
        "time_in": fields.DateTime(
            readOnly=True, description="Time when client entered the parking"
        ),
        "time_out": fields.DateTime(
            readOnly=True, description="Time when client left the parking"
        ),
    },
)


class ClientParkingResource(Resource):
    @client_parking_namespace.doc(
        description="Заезд клиента в паркинг",
        responses={
            201: ("Клиент успешно заехал в паркинг", client_parking_get_model),
            400: "Неверные данные",
            404: "Клиент или паркинг не найдены",
            409: "Клиент уже в паркинге",
            500: "Внутренняя ошибка сервера",
        },
    )
    @client_parking_namespace.expect(client_parking_post_model, validate=True)
    def post(self) -> Tuple[Dict[str, Any], int]:
        """Заезд на парковку"""
        client_parking_data = request.get_json()

        response, status = g.client_parking_service.create_client_parking(
            client_parking_data
        )
        return response, status

    @client_parking_namespace.doc(
        description="Выезд клиента из паркинга",
        responses={
            200: ("Клиент успешно выехал из паркинга", client_parking_get_model),
            400: "Клиент не в паркинге или у клиента не привязана карта",
            404: "Клиент или паркинг не найдены",
            500: "Внутренняя ошибка сервера",
        },
    )
    @client_parking_namespace.expect(client_parking_post_model, validate=True)
    def delete(self) -> Tuple[Dict[str, Any], int]:
        """Выезд с парковки"""
        client_parking_data = request.get_json()

        response, status = g.client_parking_service.leave_parking(client_parking_data)
        return response, status

    @client_parking_namespace.doc(
        description="Просмотр всех заездов/выездов",
        responses={
            200: ("Информация успешна возвращена", [client_parking_get_model]),
            500: "Внутренняя ошибка сервера",
        },
    )
    def get(self) -> Tuple[List[Dict[str, Any]], int]:
        """Просмотр всех заездов/выездов"""
        response, status = g.client_parking_service.get_list_client_parking()
        return response, status
