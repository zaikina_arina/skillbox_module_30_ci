from typing import Any, Dict, List, Tuple

from flask import g, request
from flask_restx import Resource, fields

from app.namespaces import client_namespace, parking_namespace

parking_post_model = parking_namespace.model(
    "ParkingPostModel",
    {
        "address": fields.String(required=True, description="Parking address"),
        "opened": fields.Boolean(required=True, description="Parking status"),
        "count_places": fields.Integer(
            required=True, description="Total number of parking places"
        ),
        "count_available_places": fields.Integer(
            required=True, description="Number of available parking places"
        ),
    },
)

parking_get_model = parking_namespace.clone(
    "ParkingGetModel",
    parking_post_model,
    {
        "id": fields.Integer(
            readOnly=True, description="The unique identifier of a parking"
        ),
    },
)


class ParkingListResource(Resource):
    @parking_namespace.doc(
        description="Создание нового паркинга",
        responses={
            201: ("Паркинг успешно создан", parking_post_model),
            400: "Неверные данные паркинга",
            409: "Паркинг уже существует",
            500: "Внутренняя ошибка сервера",
        },
    )
    @parking_namespace.expect(parking_post_model, validate=True)
    def post(self) -> Tuple[Dict[str, Any], int]:
        """Создание паркинга"""
        parking_data = request.get_json()

        response, status = g.parking_service.create_parking(parking_data)
        return response, status

    @client_namespace.doc(
        description="Просмотр всех паркингов",
        responses={
            200: ("Информация о паркингах успешна возвращена", [parking_get_model]),
            500: "Внутренняя ошибка сервера",
        },
    )
    def get(self) -> Tuple[List[Dict[str, Any]], int]:
        """Просмотр всех паркингов"""
        response, status = g.parking_service.get_parking_list()
        return response, status


class ParkingResource(Resource):
    @parking_namespace.doc(
        params={"parking_id": "ID паркинга"},
        description="Информация о паркинге по ID",
        responses={
            200: ("Информация о паркинге успешно возвращена", parking_get_model),
            404: "Паркинг не найден",
            500: "Внутренняя ошибка сервера",
        },
    )
    def get(self, parking_id: int) -> Tuple[List[Dict[str, Any]], int]:
        """Просмотр паркинга по ID"""
        response, status = g.parking_service.get_parking_by_id(parking_id)
        return response, status
