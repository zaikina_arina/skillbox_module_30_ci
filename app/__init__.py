from flask import Flask, g
from flask_restx import Api

from app.db import db
from app.namespaces import (client_namespace, client_parking_namespace,
                            parking_namespace)
from app.repositories.client_parking_repository import ClientParkingRepository
from app.repositories.client_repository import ClientRepository
from app.repositories.parking_repositories import ParkingRepository
from app.routers.client_parking_router import create_client_parking_router
from app.routers.client_router import create_client_router
from app.routers.parking_router import create_parking_router
from app.servces.client_parking_service import ClientParkingService
from app.servces.client_service import ClientService
from app.servces.parking_service import ParkingService


def create_app(config_class) -> Flask:
    app = Flask(__name__)
    app.config.from_object(config_class)

    api = Api(
        app,
        title="My API Parking!!!",
        version="1.0",
        description="Parking API",
        prefix="/api/v1",
    )

    db.init_app(app)

    client_repository = ClientRepository()
    parking_repository = ParkingRepository()
    client_parking_repository = ClientParkingRepository()
    client_service = ClientService(client_repository)
    parking_service = ParkingService(parking_repository)
    client_parking_service = ClientParkingService(
        client_parking_repository, client_service, parking_service
    )

    @app.before_request
    def create_service():
        g.client_service = client_service
        g.parking_service = parking_service
        g.client_parking_service = client_parking_service

    client_router = create_client_router(namespace=client_namespace)
    api.add_namespace(client_router)

    parking_router = create_parking_router(namespace=parking_namespace)
    api.add_namespace(parking_router)

    client_parking_router = create_client_parking_router(
        namespace=client_parking_namespace
    )
    api.add_namespace(client_parking_router)

    return app
