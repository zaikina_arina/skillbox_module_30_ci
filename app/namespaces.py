from flask_restx import Namespace

client_namespace = Namespace("clients", description="Операции, связанные с клиентами")
parking_namespace = Namespace("parking", description="Операции, связанные с паркингами")
client_parking_namespace = Namespace(
    "client-parking",
    description="Операции, связанные с заездом/ выездом клиентов в паркинг",
)
