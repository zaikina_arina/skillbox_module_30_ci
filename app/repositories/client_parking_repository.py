import datetime
from typing import Optional

from app.db import db
from app.models import ClientParking
from app.repositories.base_repository import BaseRepository


class ClientParkingRepository(BaseRepository):
    """
    Репозиторий для записей о парковке клиентов.
    Реализует методы для работы с БД записей о парковке.
    """

    def __init__(self):
        super().__init__(ClientParking)

    @staticmethod
    def create_entry(
            client_id: int, parking_id: int, time_in: datetime.datetime
    ) -> ClientParking:
        """
        Добавить новую запись о парковке клиента.

        :param client_id: ID клиента.
        :param parking_id: ID парковки.
        :param time_in: Время въезда на парковку.
        """
        new_entry = ClientParking(
            client_id=client_id, parking_id=parking_id, time_in=time_in
        )
        db.session.add(new_entry)
        db.session.commit()
        return new_entry

    @staticmethod
    def get_by_client(client_id: int) -> Optional[ClientParking]:
        """
        Получение записи о заезде клиента на парковку по ID клиента.

        :param client_id: ID клиента.
        """
        return db.session.query(ClientParking).filter_by(client_id=client_id).first()

    @staticmethod
    def get_active_by_client(client_id: int) -> Optional[ClientParking]:
        """
        Получение активной записи о заезде клиента в паркинг по ID клиента.

        :param client_id: ID клиента.
        """
        return (
            db.session.query(ClientParking)
            .filter(
                ClientParking.client_id == client_id, ClientParking.time_out.is_(None)
            )
            .first()
        )

    @staticmethod
    def get_active_client_in_parking(
            client_id: int, parking_id: int
    ) -> Optional[ClientParking]:
        """
        Получение записи об активном заезде клиента на парковку по ID клиента и ID парковки.

        :param client_id: ID клиента.
        :param parking_id: ID парковки.
        """
        return (
            db.session.query(ClientParking)
            .filter(
                ClientParking.client_id == client_id,
                ClientParking.parking_id == parking_id,
                ClientParking.time_out.is_(None),
            )
            .first()
        )
