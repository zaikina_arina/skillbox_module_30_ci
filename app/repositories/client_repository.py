from typing import Optional

from app.db import db
from app.models import Client
from app.repositories.base_repository import BaseRepository


class ClientRepository(BaseRepository):
    """
    Репозиторий клиентов.
    Реализует методы для работы с хранилищем данных клиентов.
    """

    def __init__(self):
        super().__init__(Client)

    @staticmethod
    def get_by_car_number(client: Client) -> Optional[Client]:
        """
        Получение клиента из БД по номеру автомобиля.

        :param client: Объект клиента.
        """
        return db.session.query(Client).filter_by(car_number=client.car_number).first()
