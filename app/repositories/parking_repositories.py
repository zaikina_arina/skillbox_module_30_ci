from typing import Optional

from app.db import db
from app.models import Parking
from app.repositories.base_repository import BaseRepository


class ParkingRepository(BaseRepository):
    """
    Репозиторий паркингов.
    Реализует методы для работы с хранилищем данных паркингов.
    """

    def __init__(self):
        super().__init__(Parking)

    @staticmethod
    def get_by_address(parking: Parking) -> Optional[Parking]:
        """
        Получение паркинга из БД по его адресу.

        :param parking: Объект паркинга.
        """
        return db.session.query(Parking).filter_by(address=parking.address).first()
