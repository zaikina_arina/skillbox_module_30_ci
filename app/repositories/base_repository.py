from typing import Generic, List, Optional, TypeVar

from app.db import db

T = TypeVar("T")


class BaseRepository(Generic[T]):
    """
    Базовый класс репозитория.
    Реализует общие методы для работы с хранилищем данных.
    """

    def __init__(self, model_class):
        self.model_class = model_class

    @staticmethod
    def create(item: T) -> T:
        """
        Добавление элемента в хранилище.

        :param item: Объект модели данных.
        :return: None
        """
        db.session.add(item)
        db.session.commit()
        return item

    def get_by_id(self, id: int) -> Optional[T]:
        """
        Получение объекта из БД по ID.

        :param id: ID объекта.
        """
        return db.session.query(self.model_class).filter_by(id=id).first()

    def get_all(self) -> List[T]:
        """
        Получение всех объектов из БД.
        """
        return db.session.query(self.model_class).all()

    @staticmethod
    def update(item: T) -> T:
        """
        Обновление объекта в БД.

        :param item: Объект модели данных.
        :return: None
        """
        db.session.commit()
        return item
