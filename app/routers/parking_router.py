from app.resources.parking_resource import ParkingListResource, ParkingResource


def create_parking_router(namespace):
    parking_namespace = namespace

    parking_namespace.add_resource(ParkingListResource, "/")
    parking_namespace.add_resource(ParkingResource, "/<int:parking_id>")

    return parking_namespace
