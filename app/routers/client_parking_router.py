from app.resources.client_parking_recourse import ClientParkingResource


def create_client_parking_router(namespace):
    client_parking_namespace = namespace

    client_parking_namespace.add_resource(ClientParkingResource, "/")

    return client_parking_namespace
