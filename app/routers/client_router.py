from app.resources.client_resource import ClientListResource, ClientResource


def create_client_router(namespace):
    client_namespace = namespace

    client_namespace.add_resource(ClientListResource, "/")
    client_namespace.add_resource(ClientResource, "/<int:client_id>")

    return client_namespace
