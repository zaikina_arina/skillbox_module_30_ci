from app.db import db


class Client(db.Model):  # type: ignore
    """Модель, описывающая клиента"""

    __tablename__ = "client"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    surname = db.Column(db.String(50), nullable=False)
    credit_card = db.Column(db.String(50))
    car_number = db.Column(db.String(10), nullable=False)

    client_parkings = db.relationship("ClientParking", back_populates="client")


class Parking(db.Model):  # type: ignore
    """Модель, описывающая паркинг"""

    __tablename__ = "parking"

    id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.String(100), nullable=False)
    opened = db.Column(db.Boolean, default=True)
    count_places = db.Column(db.Integer, nullable=False)
    count_available_places = db.Column(db.Integer, nullable=False)

    client_parkings = db.relationship("ClientParking", back_populates="parking")


class ClientParking(db.Model):  # type: ignore
    """Модель, описывающая связь между клиентами и паркингами"""

    __tablename__ = "client_parking"

    id = db.Column(db.Integer, primary_key=True)
    client_id = db.Column(db.Integer, db.ForeignKey("client.id"))
    parking_id = db.Column(db.Integer, db.ForeignKey("parking.id"))
    time_in = db.Column(db.DateTime)
    time_out = db.Column(db.DateTime)

    client = db.relationship("Client", back_populates="client_parkings")
    parking = db.relationship("Parking", back_populates="client_parkings")
