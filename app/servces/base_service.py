class BaseService:
    """
    Базовый класс сервиса для работы с сущностями.
    """

    def __init__(self, repository):
        """
        Инициализация сервиса.

        :param repository: Репозиторий, с которым будет работать сервис.
        """
        self.repository = repository

    def create(self, item):
        """
        Добавление нового элемента в репозиторий.

        :param item: Объект сущности.
        """
        return self.repository.create(item)

    def get_by_id(self, item_id: int):
        """
        Получение элемента по его ID.

        :param item_id: ID элемента.
        """
        return self.repository.get_by_id(item_id)

    def get_all(self):
        """
        Получение списка всех элементов.
        """
        return self.repository.get_all()
