from typing import Any, Dict, List, Tuple

from app.models import Client
from app.schemas.client_schema import client_schema, clients_schema
from app.servces.base_service import BaseService


class ClientService(BaseService):
    """
    Сервис для работы с клиентами.
    """

    def __init__(self, client_repository) -> None:
        super().__init__(client_repository)

    def _get_client_by_car_number(self, client: Client) -> Client:
        """
        Получение клиента по номеру автомобиля.

        :param client: Объект клиента.
        """
        return self.repository.get_by_car_number(client)

    def create_client(self, client_data: Dict[str, Any]) -> Tuple[Dict[str, Any], int]:
        """
        Создание нового клиента.

        - Если переданы невалидные данные, возвращает соответствующее сообщение и HTTP-статус 400.
        - Если клиент уже существует в БД (проверка происходит по номеру автомобиля), возвращает
        соответствующее сообщение и HTTP-статус 409.
        - В случае успеха возвращает данные вновь созданного клиента и HTTP-статус 201.

        :param client_data: Данные клиента в виде словаря.
        """
        errors = client_schema.validate(client_data)
        if errors:
            return {"message": "Invalid client data", "errors": errors}, 400

        client = client_schema.load(client_data)
        if self._get_client_by_car_number(client):
            return {"message": "Client already exists"}, 409

        new_client = self.create(client)
        return client_schema.dump(new_client), 201

    def get_client_by_id(self, client_id: int) -> Tuple[Dict[str, Any], int]:
        """
        Получение клиента по ID.

        - Если клиент с указанным ID не найден в БД, возвращает соответствующее сообщение и HTTP-статус 404.
        - Если клиент с указанным ID найден в БД, возвращает словарь с данными клиента и HTTP-статус 200.

        :param client_id: ID клиента.
        """
        client = self.get_by_id(client_id)
        if not client:
            return {"message": f"Client with ID {client_id} not found"}, 404

        return client_schema.dump(client), 200

    def get_all_clients(self) -> Tuple[List[Dict[str, Any]], int]:
        """
        Получение всех клиентов.

        Возвращает кортеж, включающий список словарей с данными клиентов и HTTP-статус 200.
        """
        clients = self.get_all()
        return clients_schema.dump(clients), 200
