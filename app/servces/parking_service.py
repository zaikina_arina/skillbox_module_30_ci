from typing import Any, Dict, List, Tuple

from app.models import Parking
from app.schemas.parking_schema import parking_list_schema, parking_schema
from app.servces.base_service import BaseService


class ParkingService(BaseService):
    """
    Сервис для работы с паркингами.
    """

    def __init__(self, parking_repository) -> None:
        super().__init__(parking_repository)

    def _get_parking_by_address(self, parking: Parking) -> Parking:
        """
        Получение клиента по номеру автомобиля.

        :param parking: Объект паркинга.
        """
        return self.repository.get_by_address(parking)

    def decrease_available_places(self, parking: Parking):
        """
        Уменьшение количества доступных мест в паркинге.

        :param parking: Объект паркинга.
        """
        parking.count_available_places -= 1
        self.repository.update(parking)

    def increase_available_places(self, parking: Parking):
        """
        Увеличение количества доступных мест в паркинге.

        :param parking: Объект паркинга.
        """
        parking.count_available_places += 1
        self.repository.update(parking)

    def create_parking(
        self, parking_data: Dict[str, Any]
    ) -> Tuple[Dict[str, Any], int]:
        """
        Создание нового паркинга.

        - Если переданы невалидные данные, возвращает соответствующее сообщение и HTTP-статус 400.
        - Если паркинг уже существует в БД (проверка происходит адресу паркинга), возвращает
        соответствующее сообщение и HTTP-статус 409.
        - В случае успеха возвращает данные вновь созданного паркинга и HTTP-статус 201.

        :param parking_data: Данные паркинга в виде словаря.
        """
        errors = parking_schema.validate(parking_data)
        if errors:
            return {"message": "Invalid parking data", "errors": errors}, 400

        parking = parking_schema.load(parking_data)
        if self._get_parking_by_address(parking):
            return {"message": "Parking already exists"}, 409

        new_parking = self.create(parking)

        return parking_schema.dump(new_parking), 201

    def get_parking_by_id(self, parking_id: int) -> Tuple[Dict[str, Any], int]:
        """
        Получение паркинга по ID.

        - Если паркинг с указанным ID не найден в БД, возвращает соответствующее сообщение и HTTP-статус 404.
        - Если паркинг с указанным ID найден в БД, возвращает словарь с данными паркинга и HTTP-статус 200.

        :param parking_id: ID паркинга.
        """
        parking = self.get_by_id(parking_id)
        if not parking:
            return {"message": f"Parking with ID {parking_id} not found"}, 404

        return parking_schema.dump(parking), 200

    def get_parking_list(self) -> Tuple[List[Dict[str, Any]], int]:
        """
        Получение всех паркингов.

        Возвращает кортеж, включающий список словарей с данными паркингов и HTTP-статус 200.
        """
        parking_list = self.get_all()
        return parking_list_schema.dump(parking_list), 200
