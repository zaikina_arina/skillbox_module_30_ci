from datetime import datetime
from typing import Any, Dict, List, Tuple

from app.schemas.client_parking_schema import (client_parking_schema,
                                               clients_parking_list_schema)
from app.servces.base_service import BaseService


class ClientParkingService(BaseService):
    """
    Сервис для работы с клиентами.
    """

    def __init__(
        self, client_parking_repository, client_service, parking_service
    ) -> None:
        super().__init__(client_parking_repository)
        self.client_parking_repository = client_parking_repository
        self.client_service = client_service
        self.parking_service = parking_service

    def create_client_parking(
        self, client_parking_data: Dict[str, int]
    ) -> Tuple[Dict[str, Any], int]:
        """
        Заезд клиента на парковку.

        - Если клиента или паркинга с указанным ID не найдено, возвращает соответствующее сообщение и
        HTTP-статус 404.
        - Если паркинг закрыт, возвращает соответствующее сообщение и HTTP-статус 400.
        - Если в паркинге нет свободных мест, возвращает соответствующее сообщение и
        HTTP-статус 404.
        - Если клиент уже находится на парковке, возвращает соответствующее сообщение
        и HTTP-статус 400.
        - При успешном заезде количество доступных мест уменьшается на 1, сохраняется дата заезда.
        Возвращается информация о заезде и HTTP-статус 200.

        :param client_parking_data: Данные заезда на парковку.
        """
        client_id = client_parking_data.get("client_id")
        parking_id = client_parking_data.get("parking_id")

        parking = self.parking_service.get_by_id(parking_id)
        if not parking:
            return {"message": f"Parking with ID {parking_id} not found"}, 404

        client = self.client_service.get_by_id(client_id)
        if not client:
            return {"message": f"Client with ID {client_id} not found"}, 404

        if not parking.opened:
            return {"message": "Parking is closed"}, 400

        if parking.count_available_places == 0:
            return {"message": "No available places on parking"}, 400

        if self.client_parking_repository.get_active_by_client(client_id):
            return {"message": "Client is already on the parking"}, 400

        self.parking_service.decrease_available_places(parking)

        client_parking = self.client_parking_repository.create_entry(
            client_id, parking_id, datetime.now()
        )

        return client_parking_schema.dump(client_parking), 201

    def leave_parking(
        self, client_parking_data: Dict[str, int]
    ) -> Tuple[Dict[str, Any], int]:
        """
        Выезд клиента с парковки.

        - Если клиента или паркинга с указанным ID не найдено, возвращает соответствующее сообщение и
        HTTP-статус 404.
        - Если нет активной записи клиента на паркинге (где time_out is null), возвращает соответствующее
        сообщение и HTTP-статус 400.
        - Если у клиента нет привязанной кредитной карты, возвращает соответствующее сообщение и
        HTTP-статус 400.
        - При успешном выезде сохраняется дата выезда, количество доступных мест увеличивается на 1.
        Возвращается информация о выезде и HTTP-статус 200.

        :param client_parking_data: Данные выезда с парковки.
        """
        client_id = client_parking_data.get("client_id")
        parking_id = client_parking_data.get("parking_id")

        parking = self.parking_service.get_by_id(parking_id)
        if not parking:
            return {"message": f"Parking with ID {parking_id} not found"}, 404

        client = self.client_service.get_by_id(client_id)
        if not client:
            return {"message": f"Client with ID {client_id} not found"}, 404

        client_parking = self.client_parking_repository.get_active_client_in_parking(
            client_id, parking_id
        )

        if not client_parking:
            return {"message": "Client is not on the parking"}, 400

        if not client.credit_card:
            return {"message": "Client does not have a linked card"}, 400

        self.parking_service.increase_available_places(parking)

        client_parking.time_out = datetime.now()
        self.client_parking_repository.update(client_parking)

        return client_parking_schema.dump(client_parking), 200

    def get_list_client_parking(self) -> Tuple[List[Dict[str, Any]], int]:
        """
        Получение всех заездов/выездов.

        Возвращает кортеж, включающий список словарей с данными заездов/выездов и HTTP-статус 200.
        """
        client_parking_list = self.get_all()
        return clients_parking_list_schema.dump(client_parking_list), 200
