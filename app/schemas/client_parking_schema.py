from marshmallow import Schema, fields, post_load

from app.models import ClientParking


class ClientParkingSchema(Schema):
    id = fields.Int(dump_only=True)
    client_id = fields.Int(required=True)
    parking_id = fields.Int(required=True)
    time_in = fields.DateTime(dump_only=True)
    time_out = fields.DateTime(dump_only=True)

    @post_load
    def make_client(self, data: dict, **kwargs) -> ClientParking:
        """
        Создание объекта заезда на парковку после десериализации данных.

        :param data: Словарь с данными заезда после десериализации.
        :param kwargs: Дополнительные аргументы.
        :return: Объект клиента.
        """
        return ClientParking(**data)


client_parking_schema = ClientParkingSchema()
clients_parking_list_schema = ClientParkingSchema(many=True)
