from marshmallow import Schema, fields, post_load

from app.models import Parking


class ParkingSchema(Schema):
    """
    Схема сериализации и десериализации данных паркинга.
    """

    id = fields.Int(dump_only=True)
    address = fields.Str(required=True)
    opened = fields.Bool(required=True)
    count_places = fields.Int(required=True)
    count_available_places = fields.Int(required=True)

    @post_load
    def make_parking(self, data: dict, **kwargs) -> Parking:
        """
        Создание объекта паркинга после десериализации данных.

        :param data: Словарь с данными паркинга после десериализации.
        :param kwargs: Дополнительные аргументы.
        :return: Объект паркинга.
        """
        return Parking(**data)


parking_schema = ParkingSchema()
parking_list_schema = ParkingSchema(many=True)
