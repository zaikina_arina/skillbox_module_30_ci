from marshmallow import Schema, fields, post_load

from app.models import Client


class ClientSchema(Schema):
    """
    Схема сериализации и десериализации данных клиента.
    """

    id = fields.Int(dump_only=True)
    name = fields.String(required=True)
    surname = fields.String(required=True)
    credit_card = fields.String(required=True)
    car_number = fields.String(required=True)

    @post_load
    def make_client(self, data: dict, **kwargs) -> Client:
        """
        Создание объекта клиента после десериализации данных.

        :param data: Словарь с данными клиента после десериализации.
        :param kwargs: Дополнительные аргументы.
        :return: Объект клиента.
        """
        return Client(**data)


client_schema = ClientSchema()
clients_schema = ClientSchema(many=True)
